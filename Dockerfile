FROM openapitools/openapi-generator-cli AS openapi_codegen
WORKDIR /root
COPY ./openapi_generator_config.yml /root/
COPY ./drax-app.api.yml /root/
RUN /usr/local/bin/docker-entrypoint.sh generate -i /root/drax-app.api.yml -g rust -o "/root/draxapp_api_codegen" \
--config /root/openapi_generator_config.yml \
--http-user-agent "DraxAppServer/unknown"
RUN /usr/local/bin/docker-entrypoint.sh generate -i /root/drax-app.api.yml -g rust-server -o "/root/draxapp_api_codegen" \
--config /root/openapi_generator_config.yml \
--http-user-agent "DraxAppServer/unknown"
RUN rm -rf "/root/draxapp_server/src/models" && mkdir -p "/root/draxapp_server/src" && mv -f "/root/draxapp_api_codegen/src/models" "/root/draxapp_server/src/" 
CMD ["sh", "-i"]

FROM rust AS rust_chef
RUN cargo install cargo-chef && rm -rf $CARGO_HOME/registry/

FROM rust_chef AS rust_gather_deps
COPY ./draxapp_server /draxapp_server
COPY --from=openapi_codegen /root/draxapp_server /draxapp_server
WORKDIR /draxapp_server
RUN cargo chef prepare --recipe-path recipe.json

FROM rust_chef as rust_build_deps
COPY --from=rust_gather_deps /draxapp_server/recipe.json /draxapp_server/recipe.json
COPY --from=rust_gather_deps /draxapp_server/Cargo.toml /draxapp_server/Cargo.toml
COPY --from=rust_gather_deps /draxapp_server/Cargo.lock /draxapp_server/Cargo.lock
COPY --from=openapi_codegen /root/draxapp_server /draxapp_server
WORKDIR /draxapp_server
RUN cargo chef cook --recipe-path recipe.json

FROM rust_build_deps AS rust_build_src
WORKDIR /draxapp_server
COPY ./draxapp_server /draxapp_server
COPY --from=openapi_codegen /root/draxapp_server /draxapp_server
RUN cargo build && mv /draxapp_server/target/debug/draxapp_server /draxapp_server/draxapp_server && rm -rf /draxapp_server/target

FROM rust AS draxapp_runtime
WORKDIR /draxapp_server
ENV PORT 5555
COPY --from=rust_build_src /draxapp_server/draxapp_server /draxapp_server/draxapp_server
COPY docker_entry_point.sh .
CMD ["/draxapp_server/docker_entry_point.sh"]
