use crate::{database, models};
use std::convert::TryFrom;

impl From<database::SessionInfo> for models::SessionInfo {
    fn from(x: database::SessionInfo) -> models::SessionInfo {
        let valid_before: u64 = x.valid_before;
        models::SessionInfo {
            session_key: x.session_key,
            auth_type: x.auth_type,
            user_id: x.user_id,
            valid_before: i32::try_from(valid_before).unwrap_or(i32::MAX),
        }
    }
}
