use clap;

#[macro_use]
mod logger;
mod api;
mod auth;
mod cors;
mod database;
mod redacted_error;
mod routes;
mod server;
mod utils;

#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate serde_json;
#[allow(dead_code)]
mod models;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let logger = &crate::logger::ROOT_SPAN;

    log_info!(logger, "Invoked draxapp_server");

    let app = clap::App::new("DraxApp");
    let matches = app
        .arg(
            clap::Arg::with_name("insecure")
                .long("insecure")
                .help("Whether to use HTTPS or not. SSL certificate is expected in ./cert.pem"),
        )
        .arg(
            clap::Arg::with_name("port")
                .long("port")
                .help("Which port to listen on")
                .takes_value(true),
        )
        .arg(
            clap::Arg::with_name("interface")
                .long("interface")
                .help("which local address to bind to")
                .takes_value(true),
        )
        .get_matches();

    let addr = matches.value_of("interface").unwrap_or("127.0.0.1");
    let port = matches
        .value_of("port")
        .unwrap_or("8080")
        .parse::<i32>()
        .expect("Bad format for port number! Usage --port 8080");
    let full_addr = format!("{}:{}", addr, port)
        .parse()
        .expect("Bad format for address! Usage --interface 127.0.0.1 --port 8080");

    server::create(full_addr, !matches.is_present("insecure"), logger).await
}
