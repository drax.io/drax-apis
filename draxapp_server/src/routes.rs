use crate::{
    auth::{self, core::SessionAuthenticationError, headers::SessionKey, oauth},
    database, logger, models,
    redacted_error::{IntoRedactedError, UserFacingError},
    utils::*,
};
use actix_web::{
    dev::HttpServiceFactory,
    http::{self, Method},
    web::{Json, Path, Query, ServiceConfig},
    FromRequest, HttpRequest, HttpResponse, Responder, Route,
};
use futures::{Future, TryFutureExt};
use std::marker::PhantomData;

pub struct DraxRouteInput {
    http_request: HttpRequest,
    payload: actix_web::web::Payload,
    logger: logger::Span<'static>,
    response_id: auth::headers::ResponseID,
}

pub struct DraxRoute<R, E, FUT, H>
where
    R: Responder + 'static,
    E: IntoRedactedError + 'static,
    FUT: Future<Output = Result<R, E>> + 'static,
    H: 'static + Fn(DraxRouteInput) -> FUT + Clone,
{
    method: http::Method,
    path: &'static str,
    handler: H,
    _marker: PhantomData<fn(DraxRouteInput) -> (R, E, FUT)>,
}

impl<R, E, FUT, H> DraxRoute<R, E, FUT, H>
where
    R: Responder + 'static,
    E: IntoRedactedError + 'static,
    FUT: Future<Output = Result<R, E>> + 'static,
    H: 'static + Fn(DraxRouteInput) -> FUT + Clone,
{
    fn factory_with(method: http::Method, path: &'static str, handler: H) -> Self {
        DraxRoute {
            method,
            path,
            handler,
            _marker: Default::default(),
        }
    }
}

impl<R, E, FUT, H> HttpServiceFactory for DraxRoute<R, E, FUT, H>
where
    R: Responder + 'static,
    E: IntoRedactedError + 'static,
    FUT: Future<Output = Result<R, E>> + 'static,
    H: 'static + Fn(DraxRouteInput) -> FUT + Clone,
{
    fn register(self, config: &mut actix_web::dev::AppService) {
        let handler = self.handler;
        let path = self.path;
        let method = self.method;
        let resource = actix_web::Resource::new(self.path).route({
            Route::new()
                .guard(actix_web::guard::Method(method.clone()))
                .to(
                    move |http_request: HttpRequest, payload: actix_web::web::Payload| {
                        let response_id = crate::auth::headers::ResponseID::generate_new();
                        let req_id_kv: logger::KVValue =
                            match crate::auth::headers::RequestID::from_request(&http_request) {
                                Ok(req_id) => req_id.to_string().into(),
                                Err(_) => "MISSING".into(),
                            };
                        let logger = log_enter!(&logger::ROOT_SPAN,
                        crate::auth::headers::REQUEST_ID_HEADER_KEY => req_id_kv,
                        crate::auth::headers::RESPONSE_ID_HEADER_KEY => response_id.to_string()
                        ; "HTTP Request {} {}", method, path);
                        handler(DraxRouteInput {
                            http_request,
                            payload,
                            logger: logger::Span::deep_clone(&logger),
                            response_id,
                        })
                        .map_err(move |redactable| {
                            log_client_warn!(
                                &logger,
                                "HTTP request FAILED with error: {}",
                                redactable
                            );
                            redactable.into_redacted_error()
                        })
                    },
                )
        });
        actix_web::dev::HttpServiceFactory::register(resource, config)
    }
}

pub fn add_drax_routes(config: &mut ServiceConfig) {
    config
        .service(DraxRoute::factory_with(Method::GET, "/logout", logout))
        .service(DraxRoute::factory_with(
            Method::GET,
            "/session",
            get_session,
        ))
        .service(DraxRoute::factory_with(
            Method::GET,
            "/authenticate/${provider}",
            authenticate,
        ));
}

//TODO: Remove everyone that uses this error
#[derive(Debug, thiserror::Error)]
pub enum NotImplementedError {
    #[error("{0}")]
    NotImplemented(String),
}

impl IntoRedactedError for NotImplementedError {
    fn into_redacted_error(&self) -> UserFacingError {
        UserFacingError::NotSupported
    }
}

pub async fn logout(input: DraxRouteInput) -> Result<impl Responder, impl IntoRedactedError> {
    let session_key = SessionKey::from_request(&input.http_request);
    // match session_key {
    //     Err(err) => Err(err),
    //     Ok(session_key) => Ok(session_key.logout(&input.logger).await.into_ok()),
    // }
    return Result::<String, _>::Err(NotImplementedError::NotImplemented(
        "logout not implemented".to_string(),
    ));
}

async fn get_session<'a>(input: DraxRouteInput) -> Result<impl Responder, impl IntoRedactedError> {
    database::SessionInfo::from_request(&input.http_request)
        .map_ok(|session_info| HttpResponse::Ok().json(models::SessionInfo::from(session_info)))
        .await
}

#[derive(Debug, thiserror::Error)]
pub enum AuthProviderError {
    #[error("The request was malformed due to error {0}")]
    RequestError(actix_web::Error),
    #[error("Could not parse the state object in the query: {0}")]
    StateParsingError(serde_json::error::Error),
    #[error("The auth provider does not exist: {0}")]
    InvalidAuthProvider(String),
    #[error("Could not authenticate session through facebook: {0}")]
    FacebookError(SessionAuthenticationError<auth::oauth::facebook::Authenticator>),
    #[error("Could not authenticate session through google: {0}")]
    GoogleError(SessionAuthenticationError<auth::oauth::google::Authenticator>),
}

impl IntoRedactedError for AuthProviderError {
    fn into_redacted_error(&self) -> UserFacingError {
        match self {
            AuthProviderError::RequestError(_) | AuthProviderError::StateParsingError(_) => {
                UserFacingError::BadRequest
            }
            AuthProviderError::InvalidAuthProvider(_) => UserFacingError::NotFound,
            AuthProviderError::GoogleError(SessionAuthenticationError::AuthenticatorError(_))
            | AuthProviderError::FacebookError(SessionAuthenticationError::AuthenticatorError(_)) => {
                UserFacingError::Unauthorized
            }
            AuthProviderError::GoogleError(SessionAuthenticationError::DatabaseError(_))
            | AuthProviderError::FacebookError(SessionAuthenticationError::DatabaseError(_)) => {
                UserFacingError::InternalServerError
            }
        }
    }
}

#[derive(Deserialize)]
struct OAuthQuery {
    state: String,
    code: String,
}

async fn authenticate(
    input: DraxRouteInput,
) -> Result<Json<models::SessionInfo>, AuthProviderError> {
    let logger = input.logger;

    let provider = Path::<String>::extract(&input.http_request)
        .await
        .map_err(|e| AuthProviderError::RequestError(e))?;

    let query = Query::<OAuthQuery>::extract(&input.http_request)
        .await
        .map_err(|e| AuthProviderError::RequestError(e))?;

    let conn = input.http_request.connection_info();

    let state: crate::models::OAuthRedirectState = serde_json::from_str(&query.state)
        .map_err(|err| AuthProviderError::StateParsingError(err))?;

    let code: String = (&query.code).to_owned();

    match provider.into_inner().as_str() {
        "facebook" => auth::core::SessionAuthentionHelper::authenticate_session::<
            auth::oauth::facebook::Authenticator,
        >(
            SessionKey(state.session),
            oauth::facebook::AuthenticationInfo {
                authorization_code: auth::oauth::AuthorizationCode::new(code),
                redirect_url: {
                    let url = format!("https://{}/authenticate/$facebook", conn.host());
                    log_info!(&logger, "OAuth redirection URL set to {}", url);
                    url
                },
            },
            &logger,
        )
        .await
        .map(|session_info| Json(session_info))
        .map_err(|e| AuthProviderError::FacebookError(e)),

        "google" => auth::core::SessionAuthentionHelper::authenticate_session::<
            auth::oauth::google::Authenticator,
        >(
            SessionKey(state.session),
            oauth::google::AuthenticationInfo {
                authorization_code: auth::oauth::AuthorizationCode::new(code),
                redirect_url: {
                    let url = format!("https://{}/authenticate/$google", conn.host());
                    log_info!(&logger, "OAuth redirection URL set to {}", url);
                    url
                },
            },
            &logger,
        )
        .await
        .map(|session_info| Json(session_info))
        .map_err(|e| AuthProviderError::GoogleError(e)),

        provider_name => Err(AuthProviderError::InvalidAuthProvider(
            provider_name.to_string(),
        )),
    }
}
