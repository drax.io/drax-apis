use crate::utils::*;
use actix_web::{HttpResponse, ResponseError};
use std::fmt;

pub trait IntoRedactedError: fmt::Display {
    fn into_redacted_error(&self) -> UserFacingError;
}

impl IntoRedactedError for Never {
    fn into_redacted_error(&self) -> UserFacingError {
        panic!("Attempting to present a physically impossible error")
    }
}

#[derive(Debug, thiserror::Error)]
pub enum UserFacingError {
    #[error("Oops, something went wrong on our side. We're looking into it.")]
    InternalServerError,
    #[error("You are not signed in.")]
    Unauthorized,
    #[error("The request is malformed.")]
    BadRequest,
    #[error("The requested page was not found.")]
    NotFound,
    #[error("This action is not supported.")]
    NotSupported,
}
// impl RedactedError for UserFacingError {}
impl ResponseError for UserFacingError {
    fn error_response(&self) -> HttpResponse {
        match self {
            UserFacingError::InternalServerError => {
                actix_web::error::ErrorInternalServerError(self.to_string())
            }
            UserFacingError::Unauthorized => actix_web::error::ErrorUnauthorized(self.to_string()),
            UserFacingError::BadRequest => actix_web::error::ErrorBadRequest(self.to_string()),
            UserFacingError::NotFound => actix_web::error::ErrorNotFound(self.to_string()),
            UserFacingError::NotSupported => {
                actix_web::error::ErrorNotImplemented(self.to_string())
            }
        }
        .error_response()
    }
}
