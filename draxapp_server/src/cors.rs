use crate::logger;
use actix_cors::Cors;

pub static CORS_ALLOWED_HOSTS: [&str; 5] = [
    "https://draxapp.katoch.co/",
    "https://harold.katoch.co:5555/",
    "https://pipedream.com/",
    "https://virtserver.swaggerhub.com/",
    "https://drax-test.herokuapp.com",
];

pub fn generate_middleware(logger: &'static logger::Span<'static>) -> Cors {
    Cors::default()
        .allowed_origin(CORS_ALLOWED_HOSTS[0])
        .allowed_origin_fn(move |origin, _req_head| match origin.to_str() {
            Ok(host) => {
                if CORS_ALLOWED_HOSTS[1..].contains(&host) {
                    true
                } else {
                    //TODO: Make the localhost exception conditional - allow only for debug builds
                    if host.starts_with("http://localhost:") {
                        log_warn!(
                            logger,
                            "Allowing CORS from {} as this is a test server",
                            &host
                        );
                        true
                    } else {
                        log_client_error!(logger, "Blocked CORS request from {}", &host);
                        false
                    }
                }
            }
            Err(_error) => false,
        })
        .allow_any_method()
        .allow_any_header()
        .supports_credentials()
        .max_age(86400)
}
