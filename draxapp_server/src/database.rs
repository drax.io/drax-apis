use crate::auth::headers::SessionKey;
use dynomite::{dynamodb::*, retry::Policy, FromAttributes, Item as DBItem, Retries};
use lazy_static::lazy_static;
/// Database read/write operations fall into two categories based on CAP constraints
/// Highly Available - Stale reads are allowed, eventually consistent (suitable for high throughput)
/// Consistent - Stale reads are not allowed, strongly consistent (suitable for transactions)
/// The two requirements may be serviced by different database softwares, or the same one which may offer tunable params
/// Consequently, sometimes data stores may only have one consistency model available while sometime they might have both
/// To be conservative, each store here assumes only one of the consistency model traits
/// Futhermore, some database softwares offer batching of queries or all-or-nothing commits of multiple writes.
/// Store classes here provide higher abstraction methods which may leverage them.
//TODO: Migrate from Rusoto DynamoDB to official AWS DynamoDB bindings when it becomes stable
// https://github.com/awslabs/aws-sdk-rust
use rusoto_core::RusotoError;
use std::collections::HashMap;
use tokio_compat_02::FutureExt;

pub trait Store {
    type Value: DBItem + Clone;
    //Identifies a particular data store (table) to read/write from/to
    const PREFIX: &'static str;
}

#[derive(Debug, thiserror::Error)]
pub enum DatabaseError {
    #[error("Encountered error while getting data from database: {0}")]
    GetItemError(RusotoError<GetItemError>),
    #[error("Encountered error while putting data to database: {0}")]
    PutItemError(RusotoError<PutItemError>),
    #[error("Encountered error while deserializing query result: ")]
    DeserializeError(dynomite::AttributeError),
    #[error("Encountered error while serializing arg for query: ")]
    SerializeError,
    #[error("DynamoDB write operation did not return the new values in API")]
    NoStoredValue,
}

lazy_static! {
    static ref DYNAMO_DB_CLIENT: dynomite::retry::RetryingDynamoDb<DynamoDbClient> =
        DynamoDbClient::new(Default::default()).with_retries(Policy::default());
}

pub async fn get_item<S: Store>(
    consistent: bool,
    key: String,
    value: String,
) -> Result<Option<S::Value>, DatabaseError> {
    let mut key_constraint = HashMap::with_capacity(1);
    key_constraint.insert(
        key,
        AttributeValue {
            s: Some(value),
            ..Default::default()
        },
    );
    let input: GetItemInput = GetItemInput {
        table_name: S::PREFIX.to_string(),
        key: key_constraint,
        consistent_read: Some(consistent),
        ..Default::default()
    };

    match DYNAMO_DB_CLIENT
        .get_item(input)
        .compat() //TODO: Remove this when getting rid of tokio 0.2
        .await
    {
        Ok(GetItemOutput { item: None, .. }) => Ok(None),
        Ok(GetItemOutput {
            item: Some(map), ..
        }) => match S::Value::from_attrs(map) {
            Ok(value) => Ok(Some(value)),
            Err(err) => Err(DatabaseError::DeserializeError(err)),
        },
        Err(error) => Err(DatabaseError::GetItemError(error)),
    }
}

pub async fn put_item<S: Store>(item: S::Value) -> Result<(), DatabaseError> {
    let input: PutItemInput = PutItemInput {
        table_name: S::PREFIX.to_string(),
        item: item.into(),
        return_values: None,
        ..Default::default()
    };

    match DYNAMO_DB_CLIENT
        .put_item(input)
        .compat() //TODO: Remove this when getting rid of tokio 0.2
        .await
    {
        Ok(_) => Ok(()),
        Err(error) => Err(DatabaseError::PutItemError(error)),
    }
}

//TODO: Check what happens when the database schema changes - will deserialization of items start failing?
#[derive(DBItem, Debug, Clone, Default)]
pub struct SessionInfo {
    /// The session key for which this data is applicable
    #[dynomite(partition_key)]
    pub session_key: String,
    /// The type of auth used for the current session, e.g. oauth provider name
    pub auth_type: String,
    /// The user_id mapped to the current session. Sessions are ephemeral, user_id is not.
    pub user_id: String,
    /// Number of seconds since January 1 1970 UTC, indicating when this token is not to be used after
    pub valid_before: u64,
}

fn get_primary_key<I: DBItem + Default>() -> String {
    I::default().key().keys().next().unwrap().to_string()
}

impl Store for SessionInfo {
    type Value = Self;
    const PREFIX: &'static str = "DraxSessionInfo";
}

impl SessionInfo {
    pub async fn get_from_session_key(
        session_key: SessionKey,
    ) -> Result<Option<Self>, DatabaseError> {
        let partition_key = get_primary_key::<Self>();
        get_item::<Self>(false, partition_key.to_string(), session_key.0).await
    }
    pub async fn put(session_info: Self) -> Result<(), DatabaseError> {
        put_item::<Self>(session_info).await
    }
}
