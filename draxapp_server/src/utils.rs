use crate::logger;
use async_trait::async_trait;
use core::pin::Pin;
use std::fmt::Debug;
use std::future::Future;

#[derive(Debug)]
pub enum Never {}
impl std::fmt::Display for Never {
    fn fmt(&self, _: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        panic!("Attempted to display physically impossible object")
    }
}
impl std::error::Error for Never {}
impl From<std::convert::Infallible> for Never {
    fn from(_: std::convert::Infallible) -> Self {
        panic!("Physically impossible error has occurred")
    }
}

pub enum Either<E1, E2> {
    A(E1),
    B(E2),
}

pub type BoxPinDynFuture<'a, T> = Pin<Box<dyn std::future::Future<Output = T> + Send + 'a>>;
pub type BoxPinDynFutureNonSend<'a, T> = Pin<Box<dyn std::future::Future<Output = T> + 'a>>;

#[async_trait]
pub trait DraxResultExtensions<T, E1> {
    fn and_then_coalescing_errors<U, E2, F: FnOnce(T) -> Result<U, E2>>(
        self,
        op: F,
    ) -> Result<U, Either<E1, E2>>;
    async fn and_then_await_coalescing_errors<
        U,
        E2,
        FUT: Future<Output = Result<U, E2>> + Send,
        F: FnOnce(T) -> FUT + Send,
    >(
        self,
        op: F,
    ) -> Result<U, Either<E1, E2>>
    where
        T: Send,
        Self: Send;
    fn into_ok(self) -> T
    where
        E1: Into<Never> + Debug;

    fn expect_with_logger(self, logger: &logger::Span<'_>, message: &str) -> T
    where
        E1: Debug;
}

#[async_trait]
impl<T, E1> DraxResultExtensions<T, E1> for Result<T, E1> {
    #[inline]
    fn and_then_coalescing_errors<U, E2, F: FnOnce(T) -> Result<U, E2>>(
        self,
        op: F,
    ) -> Result<U, Either<E1, E2>> {
        match self {
            Ok(t) => match op(t) {
                Ok(u) => Ok(u),
                Err(e) => Err(Either::B(e)),
            },
            Err(e) => Err(Either::A(e)),
        }
    }
    async fn and_then_await_coalescing_errors<
        U,
        E2,
        FUT: Future<Output = Result<U, E2>> + Send,
        F: FnOnce(T) -> FUT + Send,
    >(
        self,
        op: F,
    ) -> Result<U, Either<E1, E2>>
    where
        T: Send,
        Self: Send,
    {
        match self {
            Ok(t) => match op(t).await {
                Ok(u) => Ok(u),
                Err(e) => Err(Either::B(e)),
            },
            Err(e) => Err(Either::A(e)),
        }
    }

    fn into_ok(self) -> T
    where
        E1: Into<Never> + Debug,
    {
        self.expect("Physically impossible error has occurred")
    }

    fn expect_with_logger(self, logger: &logger::Span<'_>, message: &str) -> T
    where
        E1: Debug,
    {
        match self {
            Ok(x) => x,
            Err(e) => {
                log_critical!(logger, "{} (( error: {:?} ))", message, e);
                panic!("{} (( error: {:?} ))", message.to_string(), e)
            }
        }
    }
}

pub trait DraxVecExtensions<T> {
    fn push_get(&mut self, item: T) -> &mut T;
}

impl<T> DraxVecExtensions<T> for Vec<T> {
    fn push_get(&mut self, item: T) -> &mut T {
        self.push(item);
        self.last_mut()
            .expect("Impossible situation in DraxVecExtensions.push_get")
    }
}
