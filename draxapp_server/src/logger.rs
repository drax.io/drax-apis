use lazy_static::lazy_static;
use sloggers::{
    terminal::{Destination, TerminalLoggerBuilder},
    Build,
};
use std::{collections::HashMap, fmt, time::Instant};

pub type Level = slog::Level;

#[allow(unused_macros)]
macro_rules! log(
    ($l:expr, $lvl:expr, $($args:tt)+) => {
        $crate::logger::Span::log($l, $lvl, file!(), line!(), column!(), "", module_path!(), &format_args!($($args)*))
    };
);

#[allow(unused_macros)]
macro_rules! log_enter(
    ($l:expr, $($k:expr => $v:expr),* ; $($args:tt)+) => {{
        let mut logger = $crate::logger::Span::span($l);
        $(
            logger.add_raw_kv($k, $v.into());
        )*
        log_info!(&logger, $($args)*);
        logger
    }};
    ($l:expr ; $($args:tt)+) => {{
        let logger = $crate::logger::Span::span($l);
        log_info!(&logger, $($args)*);
        logger
    }};
);

#[allow(unused_macros)]
macro_rules! log_info(
    ($l:expr, $($args:tt)+) => {
        log!($l, $crate::logger::Level::Info, $($args)*)
    };
);

//This represents warnings which affect the server
//i.e. log_warn!() represents an warning for the entire system (e.g. server failover mechanism engaged)
#[allow(unused_macros)]
macro_rules! log_warn(
    ($l:expr, $($args:tt)+) => {
        log!($l, $crate::logger::Level::Warning, $($args)*)
    };
);

//This represents errors which affect the server
//i.e. log_error!() represents an error for the entire system (e.g. database unreachable)
#[allow(unused_macros)]
macro_rules! log_error(
    ($l:expr, $($args:tt)+) => {
        log!($l, $crate::logger::Level::Error, $($args)*)
    };
);

#[allow(unused_macros)]
macro_rules! log_debug(
    ($l:expr, $($args:tt)+) => {
        log!($l, $crate::logger::Level::Debug, $($args)*)
    };
);

#[allow(unused_macros)]
macro_rules! log_trace(
    ($l:expr, $($args:tt)+) => {
        log!($l, $crate::logger::Level::Trace, $($args)*)
    };
);

#[allow(unused_macros)]
macro_rules! log_critical(
    ($l:expr, $($args:tt)+) => {
        log!($l, $crate::logger::Level::Critical, $($args)*)
    };
);

//This represents a typical error for a client (e.g. http 401 due to expired auth header)
#[allow(unused_macros)]
macro_rules! log_client_warn(
    ($l:expr, $($args:tt)+) => {
        log!($l, $crate::logger::Level::Warning, "[CLIENT_ERROR] {}", &format_args!($($args)*))
    };
);

//This represents an atypical "investigation worthy" error for a client (e.g. malformed request, CORS blocked, etc.)
#[allow(unused_macros)]
macro_rules! log_client_error(
    ($l:expr, $($args:tt)+) => {
        log!($l, $crate::logger::Level::Error, "[CLIENT_ERROR] {}", &format_args!($($args)*))
    };
);

lazy_static! {
    static ref SLOGGER: slog::Logger = TerminalLoggerBuilder::new()
        .destination(Destination::Stderr)
        .build()
        .expect("Could not construct the terminal logger");
    pub static ref ROOT_SPAN: Span<'static> = Span::new(std::iter::empty(), 0);
}

#[derive(Debug)]
pub enum KVValue<'a> {
    Owned(String),
    Unowned(&'a str),
}
impl<'a> KVValue<'a> {
    fn as_str(&'a self) -> &'a str {
        match self {
            KVValue::Owned(x) => x,
            KVValue::Unowned(x) => x,
        }
    }
    fn as_unowned(&'a self) -> KVValue<'a> {
        KVValue::Unowned(self.as_str())
    }
}

impl<'a> From<String> for KVValue<'a> {
    fn from(s: String) -> Self {
        KVValue::Owned(s)
    }
}

impl<'a> From<&'a str> for KVValue<'a> {
    fn from(s: &'a str) -> Self {
        KVValue::Unowned(s)
    }
}

impl<'a> From<&'a String> for KVValue<'a> {
    fn from(s: &'a String) -> Self {
        Self::from(s.as_str())
    }
}

#[derive(Debug)]
pub struct Span<'a> {
    kv: HashMap<&'static str, KVValue<'a>>,
    indent: u8,
    start_time: Option<std::time::Instant>,
}

impl<'a> Span<'a> {
    pub fn log(
        &self,
        level: Level,
        file: &'static str,
        line: u32,
        column: u32,
        function: &'static str,
        module: &'static str,
        msg: &fmt::Arguments,
    ) {
        let mut kv_str_len = 0;
        for (k, v) in &self.kv {
            kv_str_len += k.len() + v.as_str().len() + 2;
        }
        let mut kv_str = String::with_capacity(kv_str_len);
        for (k, v) in &self.kv {
            kv_str.push_str(&format!("{}={} ", *k, v.as_str()));
        }
        SLOGGER.log(&slog::Record::new(
            &slog::RecordStatic {
                location: &slog::RecordLocation {
                    file,
                    line,
                    column,
                    function,
                    module,
                },
                tag: "",
                level: level,
            },
            &format_args!(
                "{:|>indent$}{}|{}",
                "",
                kv_str,
                msg,
                indent = usize::from(self.indent)
            ),
            slog::b!(),
        ))
    }
    pub fn span(&self) -> Span {
        let mut new_span = Span::derived(self);
        new_span.start_time = Some(Instant::now());
        new_span.indent += 1;
        new_span
    }
    pub fn add_kv(&mut self, key: &'static str, value: &'a str) {
        self.kv.insert(key, value.into());
    }
    pub fn add_owned_kv(&mut self, key: &'static str, value: String) {
        self.kv.insert(key, value.into());
    }
    pub fn add_raw_kv(&mut self, key: &'static str, value: KVValue<'a>) {
        self.kv.insert(key, value);
    }
    pub fn remove_kv(&mut self, key: &'static str) {
        self.kv.remove(key);
    }
    pub fn new<T: Iterator<Item = (&'static str, KVValue<'a>)>>(kv: T, indent: u8) -> Self {
        Self::new_raw(kv.collect(), indent, Some(Instant::now()))
    }
    fn new_raw(
        kv: HashMap<&'static str, KVValue<'a>>,
        indent: u8,
        start_time: Option<Instant>,
    ) -> Self {
        Span {
            indent,
            kv,
            start_time,
        }
    }
    //Make a new Span with an independent KV store, but its values still refer to the old Span so it cannot outlive it
    pub fn derived(span: &'a Span<'a>) -> Span<'a> {
        let derived_kv = span
            .kv
            .keys()
            .map(|k| (*k, span.kv.get(k).unwrap().as_unowned()))
            .collect();
        Span::new_raw(derived_kv, span.indent, None)
    }

    //Make a new Span with an independent KV which owns all its values
    pub fn deep_clone(span: &'a Span<'a>) -> Span<'static> {
        let mut cloned_kv = HashMap::new();
        for (k, v) in &span.kv {
            cloned_kv.insert(*k, KVValue::Owned(v.as_str().to_owned()));
        }
        Span::new_raw(cloned_kv, span.indent, None)
    }
}

impl<'a> Drop for Span<'a> {
    fn drop(&mut self) {
        if let Some(start_time) = self.start_time {
            self.log(
                Level::Info,
                "",
                0,
                0,
                "",
                "",
                &format_args!("SPAN ENDED in {:?}", start_time.elapsed()),
            )
        }
    }
}
