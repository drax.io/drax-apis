use super::*;
use crate::auth::core as authcore;
use crate::auth::oauth::OAuthSecret;
use crate::logger;
use crate::utils::*;
use reqwest;
use std::collections::HashMap;
use std::time;

#[derive(Deserialize, Debug, Clone)]
struct ClaimsData<'a> {
    app_id: &'a str,
    expires_at: u64,
    is_valid: bool,
    user_id: &'a str,
    #[serde(flatten, borrow)]
    extra: HashMap<&'a str, serde_json::Value>,
}

#[derive(Deserialize, Debug, Clone)]
struct Claims<'a> {
    data: ClaimsData<'a>,
    #[serde(flatten, borrow)]
    extra: HashMap<&'a str, serde_json::Value>,
}

#[derive(Deserialize)]
struct TokenUrlResponse<'a> {
    access_token: &'a str,
    token_type: &'a str,
    expires_in: u64,
}

//TODO: Keep these secret
const AUTH_TYPE: &'static str = "oauth2_facebook";
const CLIENT_SECRET: &'static str = "c827b0d9299a963fda5c3b02f0562c70";
const CLIENT_ID: &'static str = "286140706547717";

// const AUTH_URL: &'static str = "https://www.facebook.com/v11.0/dialog/oauth";
fn make_token_url(
    app_id: &str,
    redirect_uri: &str,
    app_secret: &str,
    authorization_code: &str,
) -> String {
    format!("https://graph.facebook.com/v11.0/oauth/access_token?client_id={app_id}&redirect_uri={redirect_uri}&client_secret={app_secret}&code={authorization_code}",
    app_id=app_id, redirect_uri=redirect_uri, app_secret=app_secret, authorization_code=authorization_code)
}
fn make_token_info_url(token: &str, app_id: &str, app_secret: &str) -> String {
    format!("https://graph.facebook.com/debug_token?input_token={token_to_inspect}&access_token={app_id}|{app_secret}",
    token_to_inspect=token, app_id=app_id, app_secret=app_secret)
}

//Facebook authenticator
#[derive(Debug)]
pub struct Authenticator;
impl OAuthProviderMarker for Authenticator {}

pub struct AuthenticationInfo {
    pub authorization_code: AuthorizationCode<Authenticator>,
    pub redirect_url: String,
}

impl crate::auth::oauth::OAuthProvider for Authenticator {}

impl authcore::Authenticator for Authenticator
where
    Self: Send,
{
    type Info = AuthenticationInfo;
    type Error = OAuthError;

    fn authenticate_session<'a>(
        info: Self::Info,
        logger: &'a logger::Span,
    ) -> BoxPinDynFuture<'a, Result<authcore::SessionInfo, Self::Error>> {
        let logger =
            log_enter!(&logger ; "Attempting to authenticate session via {}...", AUTH_TYPE);
        Box::pin(async move {
            log_info!(&logger, "Getting access token...");
            let token = Self::get_access_token(info, &logger).await?;
            log_info!(&logger, "Got access token, validating it...");
            Self::info_for_access_token(token, &logger).await
        })
    }
}

impl Authenticator {
    fn get_access_token<'a>(
        authentication_info: AuthenticationInfo,
        logger: &'a logger::Span,
    ) -> BoxPinDynFuture<'a, Result<OAuthSecret<Self>, OAuthError>> {
        let authorization_code_string = authentication_info.authorization_code.0;
        let redirect_url = authentication_info.redirect_url;
        let logger = log_enter!(&logger, "authorization_code" => authorization_code_string.clone(), "redirect_url" => redirect_url.clone(); "Getting access token");
        Box::pin(async move {
            let response = reqwest::get(make_token_url(
                CLIENT_ID,
                &redirect_url,
                CLIENT_SECRET,
                &authorization_code_string,
            ))
            .await;
            let response = response.map_err(|err| {
                log_client_error!(&logger, "Call to OAuth server failed with error: {}", err);
                OAuthError::Request {
                    source: Box::new(err),
                }
            })?;
            let response = response.text().await.map_err(|err| {
                log_client_error!(
                    &logger,
                    "Could not stream/utf8parse token url response body: {}",
                    err
                );
                OAuthError::Request {
                    source: Box::new(err),
                }
            })?;
            let response_json = serde_json::from_str::<TokenUrlResponse>(&response);
            let response_json = response_json.map_err(|err| {
                log_client_error!(
                    &logger,
                    "Could not parse token url response body as expected JSON: {} {}",
                    err,
                    response
                );
                OAuthError::Parse {
                    source: Box::new(err),
                    response: Box::new(response.clone()),
                }
            })?;
            Ok(OAuthSecret::new(response_json.access_token.to_string()))
        })
    }

    fn info_for_access_token<'a>(
        token: OAuthSecret<Self>,
        logger: &'a logger::Span,
    ) -> BoxPinDynFuture<'a, Result<super::SessionInfo, OAuthError>> {
        let logger = log_enter!(&logger ; "Getting info for access token...");
        Box::pin(async move {
            let logger = &logger;
            //If token is currently valid and we are its intended audience...
            let response = reqwest::get(make_token_info_url(
                &token.0.into_secret(),
                CLIENT_ID,
                CLIENT_SECRET,
            ))
            .await;
            let response = response.map_err(|err| {
                log_client_error!(&logger, "Call to OAuth server failed with error: {}", err);
                OAuthError::Request {
                    source: Box::new(err),
                }
            })?;
            let response = response.text().await.map_err(|err| {
                log_client_error!(
                    &logger,
                    "Could not stream/utf8parse token url response body: {}",
                    err
                );
                OAuthError::Request {
                    source: Box::new(err),
                }
            })?;
            let response = match serde_json::from_str::<Claims>(&response) {
                Ok(claims) => Ok(claims),
                Err(err) => {
                    log_client_error!(
                        &logger,
                        "Could not parse token url response body as expected JSON: {} {}",
                        err,
                        response
                    );
                    Err(OAuthError::Parse {
                        source: Box::new(err),
                        response: Box::new(response.clone()),
                    })
                }
            }?;

            match (
                response.data.is_valid,
                response.data.user_id,
                response.data.app_id,
                response.data.expires_at,
            ) {
                (true, user_id, client_id, expiry_date)
                    if client_id.eq(CLIENT_ID)
                        && expiry_date
                            > time::SystemTime::now()
                                .duration_since(time::UNIX_EPOCH)
                                .expect("Timestamp is from before 1970")
                                .as_secs() =>
                {
                    Ok(super::SessionInfo {
                        auth_type: AUTH_TYPE.to_string(),
                        user_id: user_id.to_string(),
                        valid_before: expiry_date,
                    })
                }
                _ => {
                    log_client_error!(logger, "The access token isn't valid: {:?}", response);
                    Err(OAuthError::Invalid {
                        info: Box::new(format!("{:?}", response)),
                    })
                }
            }
        })
    }
}
