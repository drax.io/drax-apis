use std::convert::TryInto;

use super::*;
use crate::auth::core as authcore;
use crate::logger;
use crate::utils::*;
use jsonwebtoken;
use jsonwebtoken::Algorithm;
use jsonwebtoken::DecodingKey;
use jsonwebtoken::Validation;
use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
struct Claims {
    aud: String, //Audience
    exp: usize,  //Expiration time (as UTC timestamp)
    iss: String, //Issuer
    sub: String, //Subject (whom token refers to)
}

#[derive(Debug, Serialize, Deserialize, Clone)]
struct OAuthTokenExchangeResponse {
    id_token: String, //JWT
}
impl oauth2::ExtraTokenFields for OAuthTokenExchangeResponse {}

//TODO: Right now we decrypt the token using a debugging API provided by google.
//This API is subject to throttling and is not meant for production.
//We need to instead download Google's public cert and then decode the access token (which is a JWT) ourselves

const AUTH_TYPE: &'static str = "oauth2_google";
const CLIENT_SECRET: &'static str = "_mRo7ccQKh9JUUg4hEP68Sqj";
const CLIENT_ID: &'static str =
    "999278438733-m1mk70vqunsmh0tenpt5mh4tt2rj419m.apps.googleusercontent.com";
const AUTH_URL: &'static str = "https://accounts.google.com/o/oauth2/v2/auth";
const TOKEN_URL: &'static str = "https://oauth2.googleapis.com/token";
// const DEBUG_TOKEN_URL_FORMAR_STR: &'static str =
//     "https://oauth2.googleapis.com/tokeninfo?token={token}";
const CERT_URL: &'static str = "https://www.googleapis.com/oauth2/v3/certs";
const EXPECTED_ISSUER: &'static str = "https://accounts.google.com";

struct PublicCert {
    kid: String,
    algorithm: Algorithm,
    decoding_key: DecodingKey<'static>,
}

type GoogleOIDCClient = oauth2::Client<
    oauth2::basic::BasicErrorResponse,
    oauth2::StandardTokenResponse<OAuthTokenExchangeResponse, oauth2::basic::BasicTokenType>,
    oauth2::basic::BasicTokenType,
    oauth2::basic::BasicTokenIntrospectionResponse,
    oauth2::revocation::StandardRevocableToken,
    oauth2::basic::BasicRevocationErrorResponse,
>;

lazy_static! {
    static ref OAUTH2_CLIENT: GoogleOIDCClient = GoogleOIDCClient::new(
        oauth2::ClientId::new(CLIENT_ID.to_string()),
        Some(oauth2::ClientSecret::new(CLIENT_SECRET.to_string())),
        oauth2::AuthUrl::new(AUTH_URL.to_string()).unwrap(),
        oauth2::TokenUrl::new(TOKEN_URL.to_string()).ok()
    );

    //TODO: These certs should be fetched from CERT_URL and occasionally refreshed. Not hardcoded like this.
    static ref PUBLIC_CERTS: Vec<PublicCert> = vec![
        PublicCert {
            kid: "3df0a831e093fae1e24d77d47834405f95d17b54".to_string(),
            algorithm: Algorithm::RS256,
            decoding_key: DecodingKey::from_rsa_components("psh4_fDTsNZ1JkC2BV6nsU7681neTu8D37bMwTzzT-hugnePDyLaR8a_2HnqJaABndr0793WQCkiDolIjX1wn0a6zTpdgCJL-vaFe2FqPg19TWsZ8O6oKZc_rtWu-mE8Po7RGzi9qPLv9FxJPbiGq_HnMUo0EG7J4sN3IuzbU--Wmuz8LWALwmfpE9CfOym8x5GdUzbDL1ltuC2zXCaxARDnPs6vKR6eW1MZgXqgQ6ZQO9FklH_b5WJYLBDmHAb6CguoeU-AozaoVrBHgkWoDkku7nMWoetULtgBP_tYtFM8zvJ9IDD6abZM0jl-bsHIm3XFz0MgAJ9FmPti9-iShQ",
            "AQAB"),
        },
        PublicCert {
            kid: "0fcc014f22934e47480daf107a340c22bd262b6c".to_string(),
            algorithm: Algorithm::RS256,
            decoding_key: DecodingKey::from_rsa_components("7qnlkR2Ysvik__jqELu5__2Ib4_Pix6NEmEYKY80NyIGBhUQ0QDtijFypOk3cN3aRgb1f3741vQu7PQGMr79J8jM4-sA1A6UQNmfjl-thB5JpdfQrS1n3EpsrPMUvf5w-uBMQnxmiM3hrHgjA107-UxLF_xBG8Vp_EXmZI7y6IfUwTHrNotSpLLBSNH77C8ncFcm9ADsdl-Bav2CjOaef6CpGISCscx2T4LZS6DIafU1M_xYcx3aLET9TojymjZJi2hfZDyF9x_qssrlnxqfgrI71warY8HiXsiZzOTNB6s81Fu9AaxV7YckfLHyvXwOX8lQN53c2IiAuk-T7nf69w",
            "AQAB"),
        },
    ];
}

//Google authenticator
#[derive(Debug)]
pub struct Authenticator;
impl OAuthProviderMarker for Authenticator {}

pub struct AuthenticationInfo {
    pub authorization_code: AuthorizationCode<Authenticator>,
    pub redirect_url: String,
}

impl authcore::Authenticator for Authenticator
where
    Self: Send,
{
    type Info = AuthenticationInfo;
    type Error = OAuthError;

    fn authenticate_session<'a>(
        info: Self::Info,
        logger: &'a logger::Span,
    ) -> BoxPinDynFuture<'a, Result<authcore::SessionInfo, Self::Error>> {
        let logger =
            log_enter!(&logger ; "Attempting to authenticate session via {}...", AUTH_TYPE);
        Box::pin(async move {
            log_info!(&logger, "Getting JWT...");
            let token = Self::get_jwt(info, &logger).await?;
            log_info!(&logger, "Got JWT, validating it...");
            Self::validate_jwt(token, &logger).await
        })
    }
}

impl OAuthProvider for Authenticator {}

impl Authenticator {
    fn get_jwt<'a>(
        authentication_info: AuthenticationInfo,
        logger: &'a logger::Span,
    ) -> BoxPinDynFuture<'a, Result<OAuthSecret<Self>, OAuthError>> {
        let authorization_code_string = authentication_info.authorization_code.0;
        let redirect_url = authentication_info.redirect_url;
        let logger = log_enter!(&logger, "authorization_code" => authorization_code_string.clone(), "redirect_url" => redirect_url.clone(); "Getting access token");
        let redirect_url = oauth2::RedirectUrl::new(redirect_url.clone()).map_err(|err| {
            log_client_error!(&logger, "Could not parse redirect URL with error {}", err);
            OAuthError::InternalError {
                source: Box::new(err),
            }
        });
        Box::pin(async move {
            OAUTH2_CLIENT
                .to_owned()
                .set_redirect_uri(redirect_url?)
                .exchange_code(oauth2::AuthorizationCode::new(authorization_code_string))
                .request_async(oauth2::reqwest::async_http_client)
                .await
                .map(|token_response| {
                    log_info!(&logger, "Got a token response");
                    OAuthSecret::new(token_response.extra_fields().id_token.to_owned())
                })
                .map_err(|err| {
                    log_client_error!(
                        &logger,
                        "Got error from access token exchange library: {}",
                        err
                    );
                    err.into()
                })
        })
    }

    fn validate_jwt<'a>(
        id_token: OAuthSecret<Self>,
        logger: &'a logger::Span,
    ) -> BoxPinDynFuture<'a, Result<super::SessionInfo, OAuthError>> {
        let logger = log_enter!(&logger ; "Validating access id_token...");
        Box::pin(async move {
            let logger = &logger;
            let secret = id_token.into_secret();

            //TODO: Also support other algorithms
            let mut rsa_validation_requirements = Validation::new(Algorithm::RS256);

            //By default "not before timestamp" and "expired after timestamp" are already checked
            //Additionally we want to check that we're the intended audience and the issuer is google as expected
            rsa_validation_requirements.iss = Some(EXPECTED_ISSUER.to_string());
            rsa_validation_requirements.aud =
                Some(IntoIterator::into_iter([CLIENT_ID.to_string()]).collect());

            //...and seal the variable
            let rsa_validation_requirements = rsa_validation_requirements;

            let mut result = Err(OAuthError::InvalidCert);
            for cert in PUBLIC_CERTS.iter() {
                log_info!(logger, "Attempting to validate JWT with cert {}", cert.kid);

                let validation_requirements = match cert.algorithm {
                    Algorithm::RS256 => &rsa_validation_requirements,
                    _ => {
                        log_warn!(logger, "The cert was not RS256.");
                        continue;
                    }
                };

                match jsonwebtoken::decode::<Claims>(
                    &secret,
                    &cert.decoding_key,
                    validation_requirements,
                ) {
                    Ok(info) => {
                        log_info!(logger, "Decoded and validated JWT");
                        let valid_before: u64 = info
                            .claims
                            .exp
                            .try_into()
                            .ok()
                            .expect("Could not convert usize to u64");
                        result = Ok(super::SessionInfo {
                            auth_type: AUTH_TYPE.to_string(),
                            user_id: info.claims.sub.to_string(),
                            valid_before,
                        });
                        break;
                    }
                    Err(err) => {
                        log_error!(
                            logger,
                            "Could not validate JWT with cert {} due to error: {}",
                            cert.kid,
                            err
                        );
                        match err.into_kind() {
                            jsonwebtoken::errors::ErrorKind::InvalidToken => {
                                log_error!(
                                    logger,
                                    "The access id_token isnt a JWT at all: {}",
                                    secret
                                );
                                result = Err(OAuthError::Invalid {
                                    info: Box::new(secret.clone()),
                                });
                            }

                            jsonwebtoken::errors::ErrorKind::ExpiredSignature
                            | jsonwebtoken::errors::ErrorKind::InvalidIssuer
                            | jsonwebtoken::errors::ErrorKind::InvalidAudience
                            | jsonwebtoken::errors::ErrorKind::InvalidSubject
                            | jsonwebtoken::errors::ErrorKind::ImmatureSignature => {
                                log_error!(logger, "The JWT doesn't prove enough.");
                                result = Err(OAuthError::Invalid {
                                    info: Box::new(format!(
                                        "The JWT doesn't prove enough: {}",
                                        secret
                                    )),
                                });
                            }

                            jsonwebtoken::errors::ErrorKind::Utf8(e) => {
                                log_error!(logger, "The JWT isn't base64<utf8>: {}", e);
                                result = Err(OAuthError::Parse {
                                    source: Box::new(e),
                                    response: Box::new(secret.to_owned()),
                                });
                            }
                            jsonwebtoken::errors::ErrorKind::Base64(e) => {
                                log_error!(logger, "The JWT isn't base64: {}", e);
                                result = Err(OAuthError::Parse {
                                    source: Box::new(e),
                                    response: Box::new(secret.to_owned()),
                                });
                            }
                            jsonwebtoken::errors::ErrorKind::Json(e) => {
                                log_error!(logger, "The JWT isn't base64<utf8<JSON>>: {}", e);
                                result = Err(OAuthError::Parse {
                                    source: Box::new(e),
                                    response: Box::new(secret.to_owned()),
                                });
                            }

                            jsonwebtoken::errors::ErrorKind::InvalidSignature => {
                                log_info!(logger, "The given cert was not valid for the JWT");
                                result = Err(OAuthError::Invalid {
                                    info: Box::new("The given cert was not valid for the JWT"),
                                });
                            }

                            jsonwebtoken::errors::ErrorKind::InvalidRsaKey
                            | jsonwebtoken::errors::ErrorKind::InvalidAlgorithmName
                            | jsonwebtoken::errors::ErrorKind::InvalidKeyFormat
                            | jsonwebtoken::errors::ErrorKind::InvalidAlgorithm
                            | jsonwebtoken::errors::ErrorKind::InvalidEcdsaKey => {
                                log_error!(logger, "The cert was invalid or not RS256.");
                                result = Err(OAuthError::InvalidCert);
                            }

                            jsonwebtoken::errors::ErrorKind::Crypto(e) => {
                                log_error!(logger, "Unspecified error during JWT validation");
                                result = Err(OAuthError::OtherExternalError { info: Box::new(e) });
                            }

                            _ => {
                                log_error!(logger, "Unspecified error during JWT validation");
                                result = Err(OAuthError::OtherExternalError {
                                    info: Box::new("Unknown error"),
                                });
                            }
                        }
                    }
                }
            }
            result
        })
    }
}
