use crate::redacted_error::{self, UserFacingError};
use crate::utils::*;
use actix_web::{http::HeaderValue, HttpRequest};
use std::error::Error;
use thiserror;

//TODO: Rename "Session" to "Authorization" everywhere, as session has connotations of more state than just auth state

pub const REQUEST_ID_HEADER_KEY: &str = "x-request-id";
pub const RESPONSE_ID_HEADER_KEY: &str = "x-response-id";

#[derive(Debug, Clone, Copy)]
pub struct ResponseID(uuid::Uuid);
impl ToString for ResponseID {
    //TODO: Investigate - in the actual logs it seems to be 0 instead of "MISSING"
    fn to_string(&self) -> String {
        0.to_string()
    }
}

impl ResponseID {
    pub fn generate_new() -> Self {
        //TODO: Investigate - is this even generating UUIDs? In the actual logs it seems to be 0 in the logs
        ResponseID(uuid::Uuid::new_v4())
    }
}

#[derive(Debug, thiserror::Error)]
pub enum RequestIDError {
    #[error("The request did not contain a {} header", RESPONSE_ID_HEADER_KEY)]
    HeaderMissing,
    #[error("The {} header could not be parsed: {0}", RESPONSE_ID_HEADER_KEY)]
    ParseError(String),
}

#[derive(Debug, Clone, Copy)]
pub struct RequestID(uuid::Uuid);
impl ToString for RequestID {
    fn to_string(&self) -> String {
        0.to_string()
    }
}
impl RequestID {
    pub fn from_request(req: &HttpRequest) -> Result<Self, RequestIDError> {
        match req.headers().get(REQUEST_ID_HEADER_KEY) {
            None => Err(RequestIDError::HeaderMissing),
            Some(header_value) => match header_value.to_str() {
                Err(error) => Err(RequestIDError::ParseError(error.to_string())),
                Ok(id) => match uuid::Uuid::parse_str(id) {
                    Err(error) => Err(RequestIDError::ParseError(error.to_string())),
                    Ok(id) => Ok(RequestID(id)),
                },
            },
        }
    }
}

#[derive(Clone)]
pub struct SessionKey(pub String);
impl From<SessionKey> for String {
    fn from(s: SessionKey) -> String {
        s.0
    }
}

#[derive(Debug, thiserror::Error)]
pub enum AuthHeaderError {
    #[error("the session-key was unauthorized")]
    Unauthorized,
    #[error("the auth header was malformed")]
    Malformed,
    #[error("the auth header was missing")]
    Missing,
    #[error("Unsupported auth scheme {0}")]
    UnsupportedScheme(String),
    #[error("the session-key could not be validated due to: {source}")]
    InternalError { source: Box<dyn Error + Send> },
}

impl crate::redacted_error::IntoRedactedError for AuthHeaderError {
    fn into_redacted_error(&self) -> UserFacingError {
        match self {
            AuthHeaderError::InternalError { source: _ } => {
                redacted_error::UserFacingError::InternalServerError
            }
            AuthHeaderError::Unauthorized
            | AuthHeaderError::Malformed
            | AuthHeaderError::Missing
            | AuthHeaderError::UnsupportedScheme(_) => {
                redacted_error::UserFacingError::Unauthorized
            }
        }
    }
}

impl SessionKey {
    pub fn from_request(req: &HttpRequest) -> Result<Self, AuthHeaderError> {
        let header_value = req
            .headers()
            .get("authorization")
            .ok_or(AuthHeaderError::Missing)?;
        let header_value = HeaderValue::to_str(header_value).or(Err(AuthHeaderError::Malformed))?;
        let (auth_scheme, token) = header_value
            .split_once(' ')
            .ok_or(AuthHeaderError::Malformed)?;
        if !auth_scheme.eq_ignore_ascii_case("Bearer") {
            Err(AuthHeaderError::UnsupportedScheme(auth_scheme.to_string()))?;
        }
        Ok(SessionKey(token.to_owned()))
    }
}

impl crate::database::SessionInfo {
    pub fn from_request(
        req: &HttpRequest,
    ) -> BoxPinDynFuture<'static, Result<Self, AuthHeaderError>> {
        let session_key = SessionKey::from_request(req);
        Box::pin(async move {
            let db_fetch_result =
                crate::database::SessionInfo::get_from_session_key(session_key?).await;
            match db_fetch_result {
                Ok(None) => Err(AuthHeaderError::Unauthorized),
                Ok(Some(session_info)) => Ok(session_info),
                Err(database_error) => Err(AuthHeaderError::InternalError {
                    source: Box::new(database_error),
                }),
            }
        })
    }
}
