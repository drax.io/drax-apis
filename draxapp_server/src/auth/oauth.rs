pub mod facebook;
pub mod google;

use super::core::*;
use thiserror;
pub trait OAuthProviderMarker {}

#[derive(Debug, Clone)]
struct OAuthSecret<T: OAuthProviderMarker + ?Sized + Send + Sync>(
    Secret<String>,
    std::marker::PhantomData<fn() -> T>,
);
impl<T: OAuthProvider + ?Sized + Send + Sync> OAuthSecret<T> {
    fn new(token: String) -> Self {
        OAuthSecret(Secret::new(token), Default::default())
    }
    fn secret(&self) -> &str {
        self.0.secret()
    }
    fn into_secret(self) -> String {
        self.0.into_secret()
    }
}

pub struct AuthorizationCode<T: OAuthProviderMarker + ?Sized + Send + Sync>(
    String,
    std::marker::PhantomData<fn() -> T>,
);
impl<T: OAuthProviderMarker + ?Sized + Send + Sync> AuthorizationCode<T> {
    pub fn new(code: String) -> AuthorizationCode<T> {
        AuthorizationCode(code, Default::default())
    }
}

#[derive(Debug, thiserror::Error)]
pub enum OAuthError {
    #[error("Some public certs were invalid or unsupported")]
    InvalidCert,
    #[error("The token failed verification: {info:?}")]
    Invalid {
        info: Box<dyn std::fmt::Debug + Send>,
    },
    #[error("The server responded with an error: {response:?}")]
    ServerResponse {
        response: Box<dyn std::fmt::Debug + Send>,
    },
    #[error("Could not complete network request to OAuth server: {source}")]
    Request {
        source: Box<dyn std::error::Error + Send>,
    },
    #[error("Could not parse server response: {source} for response {response:?}")]
    Parse {
        source: Box<dyn std::error::Error + Send>,
        response: Box<dyn std::fmt::Debug + Send>,
    },
    #[error("a module returned unexpected error: {info:?}")]
    OtherExternalError {
        info: Box<dyn std::fmt::Debug + Send>,
    },
    #[error("Internal error: {source}")]
    InternalError {
        source: Box<dyn std::error::Error + Send>,
    },
}

impl<E, ER> From<oauth2::RequestTokenError<E, ER>> for OAuthError
where
    E: std::error::Error + Send,
    ER: oauth2::ErrorResponse + Send,
{
    fn from(err: oauth2::RequestTokenError<E, ER>) -> Self {
        match err {
            oauth2::RequestTokenError::ServerResponse(response) => OAuthError::ServerResponse {
                response: Box::new(response),
            },
            oauth2::RequestTokenError::Request(e) => OAuthError::Request {
                source: Box::new(e),
            },
            oauth2::RequestTokenError::Parse(e, response) => OAuthError::Parse {
                source: Box::new(e),
                response: Box::new(response),
            },
            oauth2::RequestTokenError::Other(info) => OAuthError::OtherExternalError {
                info: Box::new(info),
            },
        }
    }
}

//OAuth provider that provides identity using the authorization code flow
trait OAuthProvider: Send + Sync + OAuthProviderMarker + Authenticator {}
