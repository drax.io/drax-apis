use super::headers::*;
use crate::utils::*;
use crate::{logger, redacted_error::UserFacingError};
use actix_web::Responder;
use std::{
    convert::TryFrom,
    fmt::{Debug, Display},
};

#[derive(Clone)]
pub struct Secret<T>(T);
impl<T> std::fmt::Display for Secret<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        std::fmt::Display::fmt(&"[REDACTED]", f)
    }
}
impl<T> std::fmt::Debug for Secret<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        std::fmt::Debug::fmt(&"[REDACTED]", f)
    }
}
impl<T> Secret<T> {
    pub fn secret(&self) -> &T {
        &self.0
    }
    pub fn into_secret(self) -> T {
        self.0
    }
    pub fn new(s: T) -> Self {
        Secret(s)
    }
}

impl SessionKey {
    pub async fn logout(&self, logger: &logger::Span<'_>) -> Result<impl Responder, Never> {
        //TODO: Implement this
        log_info!(logger, "Explicit logout for session key {}", self.0);
        Ok::<_, Never>(format!("Successfully logged out {}", self.0))
    }
}

#[derive(Clone)]
pub struct SessionInfo {
    pub auth_type: String,
    pub user_id: String,
    pub valid_before: u64,
}

impl SessionInfo {
    fn into_api_schema(self, session_key: SessionKey) -> crate::models::SessionInfo {
        crate::models::SessionInfo::new(
            session_key.0,
            self.auth_type,
            self.user_id,
            i32::try_from(self.valid_before).unwrap_or(i32::MAX),
        )
    }
    fn into_db_schema(self, session_key: SessionKey) -> crate::database::SessionInfo {
        crate::database::SessionInfo {
            session_key: session_key.0,
            auth_type: self.auth_type,
            user_id: self.user_id,
            valid_before: self.valid_before,
        }
    }
}

pub trait Authenticator: Send + Sync + Debug {
    type Info;
    type Error: Debug + Display;
    fn authenticate_session<'a>(
        info: Self::Info,
        logger: &'a logger::Span,
    ) -> BoxPinDynFuture<'a, Result<SessionInfo, Self::Error>>;
}

type DatabaseError = crate::database::DatabaseError;

#[derive(Debug, thiserror::Error)]
pub enum SessionAuthenticationError<A: Authenticator> {
    #[error("Could not authenticate the session key: {0}")]
    AuthenticatorError(A::Error),
    #[error("Could not connect to the database: {0}")]
    DatabaseError(DatabaseError),
}

impl<A: Authenticator> crate::redacted_error::IntoRedactedError for SessionAuthenticationError<A> {
    fn into_redacted_error(&self) -> UserFacingError {
        match self {
            SessionAuthenticationError::AuthenticatorError(_) => UserFacingError::Unauthorized,
            SessionAuthenticationError::DatabaseError(_) => UserFacingError::InternalServerError,
        }
    }
}

pub struct SessionAuthentionHelper;
impl SessionAuthentionHelper {
    pub async fn authenticate_session<A: Authenticator>(
        session: SessionKey,
        info: A::Info,
        logger: &logger::Span<'_>,
    ) -> Result<crate::models::SessionInfo, SessionAuthenticationError<A>> {
        let logger = &log_enter!(logger ; "Authenticating and mapping session...");
        let session_info = A::authenticate_session(info, logger).await.map_err(|e| {
            log_client_warn!(
                logger,
                "Could not authenticate the session due to error: {}",
                e
            );
            SessionAuthenticationError::AuthenticatorError(e)
        })?;
        Self::set_session_map(session.clone(), session_info.clone())
            .await
            .map_err(|e| SessionAuthenticationError::DatabaseError(e))?;
        Ok(session_info.into_api_schema(session))
    }

    async fn set_session_map(
        session_key: SessionKey,
        session_info: SessionInfo,
    ) -> Result<(), DatabaseError> {
        crate::database::SessionInfo::put(session_info.into_db_schema(session_key)).await
    }
}
