use crate::logger;
use crate::routes;
use actix_web::{middleware, App, HttpServer};

pub async fn create(
    addr: std::net::SocketAddr,
    https: bool,
    logger: &'static logger::Span<'static>,
) -> std::io::Result<()> {
    if https {
        panic!("HTTPS is not yet implemented. Use flag --insecure");
        //TODO: SSL
        //TODO: Integrate acme2 client to renew certs without taking the server offline
    }

    log_info!(
        logger,
        "Creating an HTTP server (Secure: {}) on address {}",
        https,
        addr
    );
    HttpServer::new(move || {
        App::new()
            .configure(routes::add_drax_routes)
            .wrap(crate::cors::generate_middleware(logger))
            .wrap(middleware::Compress::default())
            .wrap(middleware::Logger::new(
                r#"%t %a "%r" %s %b "%{x-request-id}i" "%{x-response-id}o" "%{User-Agent}i" %D"#,
            ))
    })
    .bind(addr)?
    .run()
    .await
}
