# DraxApp API Server

The API server is responsible for communicating with the Drax client app, and also for communicating with webhooks or other server-to-server communications such as auth and payments.

The API exposed by the server is expressed in the OpenAPI 3 spec in `drax-app.api.yml`

We use the OpenAPI code generator to generate models defined by the API and put them into the Rust source code. This is done locally by `gen_api.sh`.

Once the models are generated, the rust project can be built using `cd draxapp_server; cargo build`

## Linting

Use `cargo fmt` to run the code formatter.
Use `cargo check` to check for errors and warnings - please try to fix all warnings unless they are relevant reminders for future work.

## Docker

IMPORTANT NOTE: The dockerfile given builds the product in DEBUG mode. It is not suitable for production.

The entire build process of generating the models and building the rust code is also encapsulated in the Dockerfile.

`docker build -t draxapp:latest --progress=plain .`

`docker run --rm -it draxapp --insecure --port 8080`

This is slow, you would prefer having rust installed locally and generating the models only on demand.

## Heroku

For testing purposes, the app can also be deployed to Heroku

### Heroku Setup (one time)

APP_NAME="drax-test"
heroku login
heroku stack:set container
heroku config:set RUST_BACKTRACE=1
heroku config:set RUST_LOG=trace

//If using heroku git to deploy
heroku git:remote -a "$APP_NAME"

//If pushing images into the heroku registry:
heroku container:login

### Pushing to Heroku

IMPORTANT NOTE: Heroku has a 15 minute time limit on builds. If this gets hit, we need to make changes to either procompile things, or cache them somewhere with the help of a plugin. (PS: There seems to be also a way to build docker images locally and push them to heroku's registry, and directly deploy the images)

If build is less than 15 minutes, push the dockerfile and build remotely:
```
git push heroku master
```

If build is more than 15 minutes, build the image locally and push+deploy the image:
```
heroku container:push web && heroku container:release web
```

## Checking OAuth

```
https://www.facebook.com/v11.0/dialog/oauth?client_id=286140706547717&redirect_uri=https://drax-test.herokuapp.com/authenticate/$facebook&state={"session":"123"}


https://accounts.google.com/o/oauth2/v2/auth?response_type=code&scope=email&client_id=999278438733-m1mk70vqunsmh0tenpt5mh4tt2rj419m.apps.googleusercontent.com&redirect_uri=https://drax-test.herokuapp.com/authenticate/$google&state={"session":"123"}
```